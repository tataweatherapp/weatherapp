﻿using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using Autofac;
using GalaSoft.MvvmLight.Helpers;
using Android.Widget;
using Android.Views;
using System;
using Android.Content;
using WeatherApp.ViewModels;
using WeatherApp.Services;
using WeatherApp.Constants;

namespace WeatherApp.Droid.Activities
{
    [Activity(Label = "Weather", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : BaseActivity<SearchViewModel>
    {
        private EditText _searchView;
        private Button _searchButton;
        private TextView _errorTextView;
        private TextView _description;
        private ProgressBar _searchSpinner;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            new WeatherApp().Init();

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.search_view);
            ObtainViewReferences();
            ApplyBindings();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                RemoveBindings();
            }

            base.Dispose(disposing);
        }

        private void ObtainViewReferences()
        {
            _searchView = FindViewById<EditText>(Resource.Id.searchView);
            _searchButton = FindViewById<Button>(Resource.Id.searchButton);
            _errorTextView = FindViewById<TextView>(Resource.Id.errorTextView);
            _description = FindViewById<TextView>(Resource.Id.description);
            _searchSpinner = FindViewById<ProgressBar>(Resource.Id.searchSpinner);
        }

        private void ApplyBindings()
        {
            RemoveBindings();

            Bindings.Add(this.SetBinding(
                () => ViewModel.SearchText,
                () => _searchView.Text,
                BindingMode.TwoWay));

            Bindings.Add(this.SetBinding(
                () => ViewModel.Description,
                () => _description.Text));

            Bindings.Add(this.SetBinding(
                () => ViewModel.Searching,
                () => _searchSpinner.Visibility)
                .ConvertSourceToTarget(searching => searching ? ViewStates.Visible : ViewStates.Invisible));

            Bindings.Add(this.SetBinding(
                () => ViewModel.ErrorMessage,
                () => _errorTextView.Text));

            Bindings.Add(this.SetBinding(
                () => ViewModel.WeatherModel)
                .WhenSourceChanges(HandleWeatherModelChanged));

            _searchButton.SetCommand(nameof(View.Click), ViewModel.SearchCommand);
        }

        private void HandleWeatherModelChanged()
        {
            if (ViewModel.WeatherModel == null)
            {
                return;
            }

            DisplayWeatherView();
        }

        private void DisplayWeatherView()
        {
            var navigationService = WeatherApp.Container.Resolve<INavigationParameterService>();
            navigationService.PushNavigationParameters(Keys.NavKey, ViewModel.WeatherModel);
            var intent = new Intent(this, typeof(WeatherActivity));
            StartActivity(intent);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
