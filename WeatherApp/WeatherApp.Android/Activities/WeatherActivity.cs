﻿using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using GalaSoft.MvvmLight.Helpers;
using WeatherApp.ViewModels;

namespace WeatherApp.Droid.Activities
{
    [Activity(Label = "WeatherActivity")]
    public class WeatherActivity : BaseActivity<WeatherViewModel>
    {
        private TextView _cityHeaderTextView;
        private TextView _descriptionTextView;
        private TextView _temperatureTextView;
        private TextView _saveButton;
        private Button _backButton;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.weather_view);
            ObtainViewReferences();
            ApplyBindings();
        }

        private void ApplyBindings()
        {
            RemoveBindings();

            Bindings.Add(this.SetBinding(
                () => ViewModel.CityName,
                () => _cityHeaderTextView.Text));

            Bindings.Add(this.SetBinding(
                () => ViewModel.Description,
                () => _descriptionTextView.Text));

            Bindings.Add(this.SetBinding(
                () => ViewModel.Temperature,
                () => _temperatureTextView.Text));

            _saveButton.SetCommand(nameof(View.Click), ViewModel.SaveFavouriteCommand);
            _backButton.Click += OnBackButtonClicked;
        }

        private void OnBackButtonClicked(object sender, EventArgs e)
        {
            Finish();
        }

        private void ObtainViewReferences()
        {
            _cityHeaderTextView = FindViewById<TextView>(Resource.Id.cityHeader);
            _descriptionTextView = FindViewById<TextView>(Resource.Id.description);
            _temperatureTextView = FindViewById<TextView>(Resource.Id.temperature);
            _saveButton = FindViewById<Button>(Resource.Id.saveButton);
            _backButton = FindViewById<Button>(Resource.Id.backButton);
        }
    }
}
