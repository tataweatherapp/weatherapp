﻿using System.Collections.Generic;
using Android.App;
using Android.OS;
using Autofac;
using GalaSoft.MvvmLight.Helpers;
using WeatherApp.Constants;
using WeatherApp.Services;
using WeatherApp.ViewModels;

namespace WeatherApp.Droid.Activities
{
    public class BaseActivity<TViewModel> : Activity
        where TViewModel : BaseViewModel
    {
        protected List<Binding> Bindings;
        protected TViewModel ViewModel { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Bindings = new List<Binding>();
            ViewModel = WeatherApp.Container.Resolve<TViewModel>();
            InitNavigationParameters();
        }

        private void InitNavigationParameters()
        {
            var navigationService = WeatherApp.Container.Resolve<INavigationParameterService>();
            var navgationParam = navigationService.PopNavigationParameters(Keys.NavKey);
            ViewModel.Initialise(navgationParam);
        }

        protected override void OnResume()
        {
            base.OnResume();
            ViewModel.Resume();
        }

        protected void RemoveBindings()
        {
            Bindings?.ForEach(binding => binding.Detach());
            Bindings?.Clear();
        }
    }
}