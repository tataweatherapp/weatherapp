﻿using Autofac.Extras.Moq;
using Moq;
using NUnit.Framework;
using WeatherApp.Constants;
using WeatherApp.Services;

namespace WeatherApp.Tests
{
    [TestFixture]
    public class FavouritesServiceTests
    {
        private FavouritesService _sut;
        private AutoMock _mock;

        [SetUp]
        public void Setup()
        {
            _mock = AutoMock.GetLoose();
            _sut = _mock.Create<FavouritesService>();
        }

        [TearDown]
        public void TearDown()
        {
            _mock.Dispose();
            _mock = null;
            _sut = null;
        }

        [Test]
        public void FavouriteCity_WhenSet_ReturnsCorrectValue()
        {
            // Arrange
            var expectedValue = "Hello World";
            var preferencesMock = _mock.Mock<IPreferences>();
            preferencesMock.Setup(x => x.Get(It.IsAny<string>())).Returns(expectedValue);

            // Act
            var favouriteCity = _sut.FavouriteCity;

            // Assert
            Assert.AreEqual(expectedValue, favouriteCity);
        }

        [Test]
        public void FavouriteCity_WhenNotSet_ReturnsEmptyString()
        {
            // Arrange
            var preferencesMock = _mock.Mock<IPreferences>();
            preferencesMock.Setup(x => x.Get(It.IsAny<string>())).Returns(string.Empty);

            // Act
            var favouriteCity = _sut.FavouriteCity;

            // Assert
            Assert.AreEqual(string.Empty, favouriteCity);
        }

        [Test]
        public void SetFavouriteCity_PassString_SetsPreference()
        {
            // Arrange
            var expectedValue = "Hello world";
            var preferencesMock = _mock.Mock<IPreferences>();

            // Act
            _sut.SetFavouriteCity(expectedValue);

            // Assert
            preferencesMock.Verify(x => x.Set(Keys.FavouritePreferenceKey, expectedValue));
        }
    }
}
