﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Autofac.Extras.Moq;
using Moq;
using NUnit.Framework;
using WeatherApp.Exceptions;
using WeatherApp.Services;

namespace WeatherApp.Tests
{
    public class OwmWeatherServiceTests
    {
        private OwmWeatherService _sut;
        private AutoMock _mock;

        [SetUp]
        public void Setup()
        {
            _mock = AutoMock.GetLoose();
            _sut = _mock.Create<OwmWeatherService>();
        }

        [TearDown]
        public void TearDown()
        {
            _mock.Dispose();
            _mock = null;
        }

        [Test]
        public async Task GetWeatherFromCityNameAsync_GivenEmptyString_ThrowsArguementNullException()
        {
            // Arrange
            var threwException = false;
            var connectivityMock = _mock.Mock<IConnectivityService>();
            connectivityMock.Setup(x => x.IsConnected).Returns(true);

            // Act
            try
            {
                await _sut.GetFromCityNameAsync(string.Empty);
            }
            catch (ArgumentNullException)
            {
                threwException = true;
            }


            // Assert
            Assert.IsTrue(threwException, "Did not throw correct exception");
        }

        [Test]
        public async Task GetWeatherFromCityNameAsync_PassNullArg_ThrowsArguementNullException()
        {
            // Arrange
            var threwException = false;
            var connectivityMock = _mock.Mock<IConnectivityService>();
            connectivityMock.Setup(x => x.IsConnected).Returns(true);

            // Act
            try
            {
                await _sut.GetFromCityNameAsync(null);
            }
            catch (ArgumentNullException)
            {
                threwException = true;
            }


            // Assert
            Assert.IsTrue(threwException, "Did not throw correct exception");
        }

        [Test]
        public async Task GetWeatherFromCityNameAsync_GivenValidString_ReturnsWeatherModel()
        {
            // Arrange
            var expectedDescription = "cloudy";
            var expectedTemp = 28.3;

            var httpResponseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            httpResponseMessage.Content = GenerateWeatherContent(expectedDescription, expectedTemp);

            var httpClientMock = _mock.Mock<IHttpClient>();
            httpClientMock.Setup(x => x.GetAsync(It.IsAny<string>())).ReturnsAsync(httpResponseMessage);

            var connectivityMock = _mock.Mock<IConnectivityService>();
            connectivityMock.Setup(x => x.IsConnected).Returns(true);

            // Act
            var model = await _sut.GetFromCityNameAsync("London");

            // Assert
            Assert.IsNotNull(model);
            Assert.AreEqual(expectedDescription, model.Weather.First().Description);
            Assert.AreEqual(expectedTemp, model.Main.Temp);
        }

        [Test]
        public async Task GetWeatherFromCityNameAsync_HttpRequestExceptionThrown_ReturnsNull()
        {
            // Arrange
            bool threwException = false;

            var httpClientMock = _mock.Mock<IHttpClient>();
            httpClientMock.Setup(x => x.GetAsync(It.IsAny<string>())).Throws(new HttpRequestException());

            var connectivityMock = _mock.Mock<IConnectivityService>();
            connectivityMock.Setup(x => x.IsConnected).Returns(true);

            // Act
            try
            {
                var model = await _sut.GetFromCityNameAsync("London");
            }
            catch (OwmWeatherServiceException)
            {
                threwException = true;
            }
            

            // Assert
            Assert.IsTrue(threwException);
        }

        [Test]
        public async Task GetWeatherFromCityNameAsync_NoInternetConnection_ThrowNoInternetConnectionException()
        {
            // Arrange
            bool threwException = false;

            var connectivityMock = _mock.Mock<IConnectivityService>();
            connectivityMock.Setup(x => x.IsConnected).Returns(false);

            // Act
            try
            {
                var model = await _sut.GetFromCityNameAsync("London");
            }
            catch (NoConnectionException)
            {
                threwException = true;
            }


            // Assert
            Assert.IsTrue(threwException);
        }

        private HttpContent GenerateWeatherContent(string description, double temp)
        {
            return new StringContent($"{{\"weather\":[{{\"description\":\"{description}\"}}],\"main\":{{\"temp\":{temp}}}}}");
        }
    }
}
