﻿using Autofac.Extras.Moq;
using Moq;
using NUnit.Framework;
using WeatherApp.Exceptions;
using WeatherApp.Models;
using WeatherApp.Services;
using WeatherApp.ViewModels;

namespace WeatherApp.Tests
{
    [TestFixture]
    public class SearchViewModelTests
    {
        private SearchViewModel _sut;
        private AutoMock _mock;

        [SetUp]
        public void Setup()
        {
            _mock = AutoMock.GetLoose();
            _sut = _mock.Create<SearchViewModel>();
        }

        [TearDown]
        public void TearDown()
        {
            _mock.Dispose();
            _mock = null;
        }

        [Test]
        public void Resume_WhenConstructedAndFavouriteCitySet_PopulatesSearchText()
        {
            // Arrange
            var favouriteCity = "London";
            var preferencesMock = _mock.Mock<IPreferences>();
            preferencesMock.Setup(x => x.Get(It.IsAny<string>())).Returns(favouriteCity);

            // Act
            _sut.Resume();

            // Assert
            Assert.AreEqual(favouriteCity, _sut.SearchText);
        }

        [TestCase("London", true)]
        [TestCase("", false)]
        public void SearchCommand_DependingOnSearchTextFilled_CanExecute(string searchText, bool expectedCanExecute)
        {
            // Arrange
            _sut.SearchText = searchText;

            // Act
            var canExecute = _sut.SearchCommand.CanExecute(null);

            // Assert
            Assert.AreEqual(expectedCanExecute, canExecute);
        }

        [Test]
        public void SearchCommand_ValidCityName_WeatherModelIsSet()
        {
            // Arrange
            var expectedWeatherModel = new Models.WeatherModel { Main = new Main { Temp = 22.9 }, Weather = new Models.Weather[] { new Models.Weather { Description = "Cloudy" } } };
            var weatherServiceMock = _mock.Mock<IWeatherService>();
            weatherServiceMock.Setup(x => x.GetFromCityNameAsync(It.IsAny<string>())).ReturnsAsync(expectedWeatherModel);
            _sut.SearchText = "Hello world";

            // Act
            _sut.SearchCommand.Execute(null);

            // Assert
            Assert.AreEqual(expectedWeatherModel, _sut.WeatherModel);
        }

        [Test]
        public void SearchCommand_NoInternetConnection_ErrorMessageHasCorrectText()
        {
            // Arrange
            var expectedErrorMessage = "No Internet Connection.";
            var weatherServiceMock = _mock.Mock<IWeatherService>();
            weatherServiceMock.Setup(x => x.GetFromCityNameAsync(It.IsAny<string>())).ThrowsAsync(new NoConnectionException());

            _sut.SearchText = "Hello world";

            // Act
            _sut.SearchCommand.Execute(null);

            // Assert
            Assert.AreEqual(_sut.ErrorMessage, expectedErrorMessage);
        }

        [Test]
        public void SearchCommand_ThrowsHttpRequestException_ErrorMessageHasCorrectText()
        {
            // Arrange
            var expectedErrorMessage = "Something went wrong, please try again.";
            var weatherServiceMock = _mock.Mock<IWeatherService>();
            weatherServiceMock.Setup(x => x.GetFromCityNameAsync(It.IsAny<string>())).ThrowsAsync(new OwmWeatherServiceException());

            _sut.SearchText = "Hello world";

            // Act
            _sut.SearchCommand.Execute(null);

            // Assert
            Assert.AreEqual(expectedErrorMessage, _sut.ErrorMessage);
        }
    }
}
