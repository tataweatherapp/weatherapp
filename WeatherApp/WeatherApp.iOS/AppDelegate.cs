﻿using Foundation;
using UIKit;
using WeatherApp.iOS.ViewControllers;

namespace WeatherApp.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {
        public override UIWindow Window
        {
            get;
            set;
        }

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            new WeatherApp().Init();

            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            var controller = new UINavigationController();
            controller.View.BackgroundColor = UIColor.LightGray;

            var searchViewController = new SearchViewController();
            controller.PushViewController(searchViewController, false);
            searchViewController.DidMoveToParentViewController(controller);

            Window.RootViewController = controller;

            Window.MakeKeyAndVisible();

            return true;
        }
    }
}
