﻿using System;
using GalaSoft.MvvmLight.Helpers;
using UIKit;
using WeatherApp.iOS.ViewControllers;
using WeatherApp.ViewModels;

namespace WeatherApp.iOS.Controllers
{
    public partial class WeatherViewController : BaseViewController<WeatherViewModel>
    {
        public WeatherViewController()
            : base("WeatherViewController")
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ApplyBindings();
        }

        private void ApplyBindings()
        {
            RemoveBindings();

            Bindings.Add(this.SetBinding(
                () => ViewModel.CityName,
                () => CityLabel.Text));

            Bindings.Add(this.SetBinding(
                () => ViewModel.Description,
                () => DescriptionLabel.Text));

            Bindings.Add(this.SetBinding(
                () => ViewModel.Temperature,
                () => TemperatureLabel.Text));

            BackButton.AddTarget(HandleBackButtonTapped, UIControlEvent.TouchUpInside);

            SaveFavouriteCityButton.SetCommand(ViewModel.SaveFavouriteCommand);
        }

        private void HandleBackButtonTapped(object sender, EventArgs e)
        {
            NavigationController.PopViewController(false);
        }
    }
}