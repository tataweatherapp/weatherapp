// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace WeatherApp.iOS.Controllers
{
	[Register ("WeatherViewController")]
	partial class WeatherViewController
	{
		[Outlet]
		UIKit.UIButton BackButton { get; set; }

		[Outlet]
		UIKit.UILabel CityLabel { get; set; }

		[Outlet]
		UIKit.UILabel DescriptionLabel { get; set; }

		[Outlet]
		UIKit.UIButton SaveFavouriteCityButton { get; set; }

		[Outlet]
		UIKit.UILabel TemperatureLabel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (CityLabel != null) {
				CityLabel.Dispose ();
				CityLabel = null;
			}

			if (BackButton != null) {
				BackButton.Dispose ();
				BackButton = null;
			}

			if (DescriptionLabel != null) {
				DescriptionLabel.Dispose ();
				DescriptionLabel = null;
			}

			if (SaveFavouriteCityButton != null) {
				SaveFavouriteCityButton.Dispose ();
				SaveFavouriteCityButton = null;
			}

			if (TemperatureLabel != null) {
				TemperatureLabel.Dispose ();
				TemperatureLabel = null;
			}
		}
	}
}
