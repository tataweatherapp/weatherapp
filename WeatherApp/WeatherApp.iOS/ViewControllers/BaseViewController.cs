﻿using System.Collections.Generic;
using Autofac;
using GalaSoft.MvvmLight.Helpers;
using UIKit;
using WeatherApp.Constants;
using WeatherApp.Services;
using WeatherApp.ViewModels;

namespace WeatherApp.iOS.ViewControllers
{
    public class BaseViewController<TViewModel> : UIViewController
        where TViewModel : BaseViewModel
    {
        protected TViewModel ViewModel;
        protected List<Binding> Bindings;

        public BaseViewController(string nibName)
            : base(nibName, null)
        {
            Bindings = new List<Binding>();
            ViewModel = WeatherApp.Container.Resolve<TViewModel>();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            NavigationController.NavigationBarHidden = true;
            InitialiseNavigationParameters();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            ViewModel.Resume();
        }

        protected void RemoveBindings()
        {
            Bindings?.ForEach(binding => binding.Detach());
            Bindings?.Clear();
        }

        private void InitialiseNavigationParameters()
        {
            var navigationService = WeatherApp.Container.Resolve<INavigationParameterService>();
            var navgationParam = navigationService.PopNavigationParameters(Keys.NavKey);
            ViewModel.Initialise(navgationParam);
        }
    }
}