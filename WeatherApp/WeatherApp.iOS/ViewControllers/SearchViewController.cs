﻿using Autofac;
using GalaSoft.MvvmLight.Helpers;
using WeatherApp.Constants;
using WeatherApp.iOS.Controllers;
using WeatherApp.Services;
using WeatherApp.ViewModels;

namespace WeatherApp.iOS.ViewControllers
{
    public partial class SearchViewController : BaseViewController<SearchViewModel>
    {
        public SearchViewController()
            : base("SearchViewController")
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ApplyBindings();
        }

        private void ApplyBindings()
        {
            RemoveBindings();

            Bindings.Add(this.SetBinding(
                () => ViewModel.SearchText,
                () => CityTextField.Text,
                BindingMode.TwoWay));

            Bindings.Add(this.SetBinding(
                () => ViewModel.Description,
                () => DescriptionLabel.Text));

            Bindings.Add(this.SetBinding(
                () => ViewModel.Searching,
                () => SearchIndicator.Hidden)
                .ConvertSourceToTarget(x => !x));

            Bindings.Add(this.SetBinding(
                () => ViewModel.ErrorMessage,
                () => ErrorLabel.Text));

            Bindings.Add(this.SetBinding(
                () => ViewModel.WeatherModel)
                .WhenSourceChanges(HandleWeatherModelChanged));

            SearchButton.SetCommand(ViewModel.SearchCommand);
        }

        private void HandleWeatherModelChanged()
        {
            if (ViewModel.WeatherModel == null)
            {
                return;
            }

            NavigateToWeather();
        }

        private void NavigateToWeather()
        {
            var navigationService = WeatherApp.Container.Resolve<INavigationParameterService>();
            navigationService.PushNavigationParameters(Keys.NavKey, ViewModel.WeatherModel);

            var weatherViewController = new WeatherViewController();
            NavigationController.PushViewController(weatherViewController, true);
        }
    }
}