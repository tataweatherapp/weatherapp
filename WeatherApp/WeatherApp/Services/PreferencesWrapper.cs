﻿using Xamarin.Essentials;

namespace WeatherApp.Services
{
    public class PreferencesWrapper : IPreferences
    {
        public string Get(string key)
        {
            return Preferences.Get(key, string.Empty);
        }

        public void Set(string key, string value)
        {
            Preferences.Set(key, value);
        }
    }
}
