﻿namespace WeatherApp.Services
{
    public interface INavigationParameterService
    {
        void PushNavigationParameters(string key, object param);
        object PopNavigationParameters(string key);
    }
}
