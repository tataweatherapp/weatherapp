﻿namespace WeatherApp.Services
{
    public interface IConnectivityService
    {
        bool IsConnected { get; }
    }
}