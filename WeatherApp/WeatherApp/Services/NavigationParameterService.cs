﻿using System.Collections.Generic;

namespace WeatherApp.Services
{
    public class NavigationParameterService : INavigationParameterService
    {
        private readonly Dictionary<string, object> _parametersByKey;

        public NavigationParameterService()
        {
            _parametersByKey = new Dictionary<string, object>();
        }

        public void PushNavigationParameters(string key, object param)
        {
            _parametersByKey.Add(key, param);
        }

        public object PopNavigationParameters(string key)
        {
            _parametersByKey.TryGetValue(key, out var param);
            _parametersByKey.Remove(key);
            return param;
        }
    }
}
