﻿namespace WeatherApp.Services
{
    public interface IFavouritesService
    {
        string FavouriteCity { get; }

        void SetFavouriteCity(string cityName);
    }
}
