﻿namespace WeatherApp.Services
{
    public interface IPreferences
    {
        string Get(string key);

        void Set(string key, string value);
    }
}