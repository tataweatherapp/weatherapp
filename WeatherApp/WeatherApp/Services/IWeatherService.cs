﻿using System.Threading.Tasks;
using WeatherApp.Models;

namespace WeatherApp.Services
{
    public interface IWeatherService
    {
        Task<WeatherModel> GetFromCityNameAsync(string cityName);
    }
}
