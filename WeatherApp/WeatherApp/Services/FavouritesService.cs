﻿using WeatherApp.Constants;

namespace WeatherApp.Services
{
    public class FavouritesService : IFavouritesService
    {
        private readonly IPreferences _preferences;

        public FavouritesService(IPreferences preferences)
        {
            _preferences = preferences;
        }

        public string FavouriteCity => _preferences.Get(Keys.FavouritePreferenceKey);

        public void SetFavouriteCity(string cityName)
        {
            _preferences.Set(Keys.FavouritePreferenceKey, cityName);
        }
    }
}
