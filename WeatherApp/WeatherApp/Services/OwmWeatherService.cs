﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WeatherApp.Constants;
using WeatherApp.Exceptions;
using WeatherApp.Models;

namespace WeatherApp.Services
{
    public class OwmWeatherService : IWeatherService
    {
        private readonly IHttpClient _httpClient;
        private readonly IConnectivityService _connectivityService;
        private const string _cityQuery = "?q={0}&units=metric&appid={1}";
        
        public OwmWeatherService(
            IHttpClient httpClient,
            IConnectivityService connectivityService)
        {
            _httpClient = httpClient;
            _connectivityService = connectivityService;
        }

        public async Task<WeatherModel> GetFromCityNameAsync(string cityName)
        {
            if (string.IsNullOrEmpty(cityName))
            {
                throw new ArgumentNullException("cityName cannot be null or empty");
            }

            var path = string.Format(_cityQuery, cityName, Keys.WeatherKey);

            if (!_connectivityService.IsConnected)
            {
                throw new NoConnectionException();
            }

            try
            {
                var result = await _httpClient.GetAsync(Path.Combine(Urls.OpenWeatherMapApi, path));
                if (result.IsSuccessStatusCode)
                {
                    var content = await result.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<WeatherModel>(content);
                }

                switch (result.StatusCode)
                {
                    case System.Net.HttpStatusCode.NotFound:
                        throw new NoCityFoundException();
                    default:
                        throw new HttpRequestException();
                }
            }
            catch (Exception e) when (e is WebException
                                   || e is HttpRequestException)
            {
                throw new OwmWeatherServiceException();
            }
        }
    }
}
