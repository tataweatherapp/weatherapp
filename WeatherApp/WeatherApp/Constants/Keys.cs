﻿namespace WeatherApp.Constants
{
    public class Keys
    {
        internal static string WeatherKey = "5e517d01167ebb6930e4a4bbf874ec50";

        public static string FavouritePreferenceKey = "fav";

        public static string NavKey = "nav_key";
    }
}
