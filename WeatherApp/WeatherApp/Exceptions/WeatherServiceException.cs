﻿using System;
using System.Runtime.Serialization;

namespace WeatherApp.Exceptions
{
    [Serializable]
    public class OwmWeatherServiceException : Exception
    {
        public OwmWeatherServiceException()
        {
        }

        public OwmWeatherServiceException(string message) : base(message)
        {
        }

        public OwmWeatherServiceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected OwmWeatherServiceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}