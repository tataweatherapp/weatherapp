﻿using System;
using System.Runtime.Serialization;

namespace WeatherApp.Exceptions
{
    [Serializable]
    internal class NoCityFoundException : Exception
    {
        public NoCityFoundException()
        {
        }

        public NoCityFoundException(string message) : base(message)
        {
        }

        public NoCityFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoCityFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}