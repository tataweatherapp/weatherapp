﻿namespace WeatherApp.Models
{
    public class WeatherModel
    {
        public string Name { get; set; }

        public Weather[] Weather { get; set; }

        public Main Main { get; set; }
    }

    public class Weather
    {
        public string Description { get; set; }
    }

    public class Main
    {
        public double Temp { get; set; }
    }
}