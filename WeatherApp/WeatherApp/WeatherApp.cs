﻿using Autofac;
using WeatherApp.Services;
using WeatherApp.ViewModels;

namespace WeatherApp
{
    public class WeatherApp
    {
        public static IContainer Container;

        public void Init()
        {
            // configure ioc container
            var container = new ContainerBuilder();
            container.RegisterType<NavigationParameterService>().As<INavigationParameterService>().SingleInstance();
            container.RegisterType<ConnectivityService>().As<IConnectivityService>();
            container.RegisterType<HttpClientWrapper>().As<IHttpClient>();
            container.RegisterType<OwmWeatherService>().As<IWeatherService>();
            container.RegisterType<PreferencesWrapper>().As<IPreferences>();
            container.RegisterType<SearchViewModel>().As<SearchViewModel>();
            container.RegisterType<WeatherViewModel>().As<WeatherViewModel>();

            Container = container.Build();
        }
    }
}
