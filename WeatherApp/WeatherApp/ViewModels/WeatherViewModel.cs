﻿using System.Linq;
using GalaSoft.MvvmLight.Command;
using WeatherApp.Constants;
using WeatherApp.Models;
using WeatherApp.Services;

namespace WeatherApp.ViewModels
{
    public class WeatherViewModel : BaseViewModel
    {
        private RelayCommand _saveFavouriteCommand;
        private IPreferences _preferences;
        private WeatherModel _weatherViewModel;

        public WeatherViewModel(IPreferences preferences)
        {
            _preferences = preferences;
        }

        public WeatherModel WeatherModel { get; set; }

        public RelayCommand SaveFavouriteCommand => _saveFavouriteCommand ?? (_saveFavouriteCommand = new RelayCommand(HandleSaveFavouriteCommand, true));

        public string CityName { get; set; }

        public string Description { get; set; }

        public string Temperature { get; set; }

        private void HandleSaveFavouriteCommand()
        {
            _preferences.Set(Keys.FavouritePreferenceKey, CityName);
        }

        public override void Initialise(object parameter)
        {
            base.Initialise(parameter);

            _weatherViewModel = (WeatherModel)parameter;
            CityName = _weatherViewModel.Name;
            Temperature = _weatherViewModel.Main.Temp.ToString();
            Description = _weatherViewModel.Weather.First().Description;
        }
    }
}