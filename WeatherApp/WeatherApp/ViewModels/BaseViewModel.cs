﻿using System;
using System.ComponentModel;

namespace WeatherApp.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void Resume() { }

        public virtual void Initialise(object parameter) { }
    }
}