﻿using System.Threading.Tasks;
using PropertyChanged;
using GalaSoft.MvvmLight.Command;
using WeatherApp.Constants;
using WeatherApp.Exceptions;
using WeatherApp.Models;
using WeatherApp.Services;

namespace WeatherApp.ViewModels
{
    public class SearchViewModel : BaseViewModel
    {
        private readonly IWeatherService _weatherService;
        private RelayCommand _searchCommand;
        private IPreferences _preferences;

        public SearchViewModel(
            IWeatherService weatherService,
            IPreferences preferences)
        {
            _weatherService = weatherService;
            _preferences = preferences;
            Description = "Type a City name to see the weather.";
        }

        public RelayCommand SearchCommand => _searchCommand ?? (_searchCommand = new RelayCommand(() => HandleSearchCommand(), HandleCanExecute, true));

        public string Description { get; set; }

        public bool Searching { get; private set; }

        [OnChangedMethod(nameof(OnSearchTextChanged))]
        public string SearchText { get; set; }

        public string ErrorMessage { get; private set; }

        public WeatherModel WeatherModel { get; private set; }

        public override void Resume()
        {
            base.Resume();
            PopulateWithFavouriteIfNotEmpty();
        }

        public void OnSearchTextChanged()
        {
            ErrorMessage = string.Empty;
            SearchCommand.RaiseCanExecuteChanged();
        }

        private bool HandleCanExecute() => !Searching && !string.IsNullOrEmpty(SearchText);

        private async Task HandleSearchCommand()
        {
            try
            {
                if (Searching)
                {
                    return;
                }

                Searching = true;
                SearchCommand.RaiseCanExecuteChanged();

                var weatherModel = await _weatherService.GetFromCityNameAsync(SearchText);
                if (weatherModel != null)
                {
                    WeatherModel = weatherModel;
                }
            }
            catch (NoConnectionException)
            {
                ErrorMessage = "No Internet Connection.";
            }
            catch (OwmWeatherServiceException)
            {
                ErrorMessage = "Something went wrong, please try again.";
            }
            catch(NoCityFoundException)
            {
                ErrorMessage = "No City Found.";
            }

            Searching = false;
            SearchCommand.RaiseCanExecuteChanged();
        }

        private void PopulateWithFavouriteIfNotEmpty()
        {
            if (!string.IsNullOrEmpty(SearchText))
            {
                return;
            }

            SearchText = _preferences.Get(Keys.FavouritePreferenceKey);
        }
    }
}
